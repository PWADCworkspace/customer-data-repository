//https://bezkoder.com/node-js-rest-api-express-mysql
// Import express and body-parser modules. 
const express = require("express"); // Express is for building the Rest apis
const bodyParser = require("body-parser"); //body-parser helps to parse the request and create the req.body object that we will need to access in our routes.

const app = express(); //create an Express app

//add body-parser middlewares using app.use() method
app.use(bodyParser.json());  // parse requests of content-type: application/json
app.use(bodyParser.urlencoded({ extended: true })); // parse requests of content-type: application/x-www-form-urlencoded

// simple route
app.get("/portal/", (req, res) => {
  res.json({ message: "Welcome to the PWADC Customer Portal Web API." });
});

//Register Routes
require("./app/routes/stores.routes.js")(app); //register the store API routes

var port = process.env.PORT || 3001;

// set port, listen for requests
app.listen(port, () => {
  console.info(`Server is running on port ${port}.`);
});