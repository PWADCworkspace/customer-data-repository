const Store = require("../models/store.model.js");

// Retrieve all stores from the database
exports.findAll = (req, res) => {
    Store.findAll((err, data) => {
        if (err) {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving stores."
                });
        }
        else {
            res.send(data);
        }
    });
};

// Find a single store with storeId parameter
exports.findOne = (req, res) => {
    Store.findById(req.params.storeId, (err, data) => {
            if (err) {
                if (err.kind === "not_found") {
                    res.status(404).send({
                        message: `Unable to locate store id ${req.params.storeId}.`
                    });
                } else {
                    res.status(500).send({
                        message: "Error retrieving store with id " + req.params.storeId
                    });
                }
            } else res.send(data);
        });  
};