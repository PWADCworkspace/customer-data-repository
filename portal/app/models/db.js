const mysql = require('mysql2');
const { Client } = require('ssh2');
const dbConfig = require("../config/db.config.js"); //uses the configuration from db.config.js to create a database connection

// create an instance of SSH Client
const sshClient = new Client();

const dbServer = {
  host: dbConfig.DB_HOST,
  port: dbConfig.DB_PORT,
  user: dbConfig.DB_USER,
  password: dbConfig.DB_PASSWORD,
  database: dbConfig.DB
}

const tunnelConfig = {
  host: dbConfig.SSH_HOST,
  port: dbConfig.SSH_PORT,
  username: dbConfig.SSH_USER,
  password: dbConfig.SSH_PASSWORD
}

const forwardConfig = {
  srcHost: '127.0.0.1',
  srcPort: 3306,
  dstHost: dbServer.host,
  dstPort: dbServer.port
};

const SSHConnection = new Promise((resolve, reject) => {
  sshClient.on('ready', () => {
      sshClient.forwardOut(
      forwardConfig.srcHost,
      forwardConfig.srcPort,
      forwardConfig.dstHost,
      forwardConfig.dstPort,
      (err, stream) => {
           if (err) reject(err);
           const updatedDbServer = {
               ...dbServer,
               stream
          };
        
         const connection =  mysql.createConnection(updatedDbServer);
         connection.connect((error) => {
          if (error) {
              reject(error);
          }
          resolve(connection);
          });
      });
  }).connect(tunnelConfig);
});

module.exports = SSHConnection;