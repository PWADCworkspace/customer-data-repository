const database = require("./db.js");

//constructor
const Store = function(store){
    this.id = store.id;
    this.name = store.name;
    this.store_number = store.store_number;
    this.created_at = store.created_at;
    this.updated_at = store.updated_at;
    this.deleted_at = store.deleted_at;
}

Store.findAll = result => {
    database.then(function(connection){
        connection.query("SELECT * FROM locations", (err, res) => {
            if (err) {
            console.error("error: ", err);
            result(null, err);
            return;
            }
    
            console.log("stores: ", res);
            result(null, res);
        });
    });
};

Store.findById = (storeId, result) => {

    database.then(function(connection){

        connection.query(`SELECT * FROM locations WHERE store_number = ${storeId}`, (err, res) => {
            if (err) {
                console.error("error: ", err);
                result(err, null);
                return;
            }
    
            if (res.length) {
                console.log("found store: ", res[0]);
                result(null, res[0]);
                return;
            }
    
            // not found store with the id
            result({ kind: "not_found" }, null);
        });
    });
};

module.exports = Store;