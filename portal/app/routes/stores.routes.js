module.exports = app => {
    const stores = require("../controllers/store.controller.js");
  
    // Retrieve all stores
    app.get("/portal/stores", stores.findAll);
  
    // Retrieve a single store with storeId
    app.get("/portal/stores/:storeId", stores.findOne);
  };