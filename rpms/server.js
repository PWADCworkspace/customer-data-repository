//https://bezkoder.com/node-js-rest-api-express-mysql
// Import express and body-parser modules. 
const express = require("express"); // Express is for building the Rest apis
const bodyParser = require("body-parser"); //body-parser helps to parse the request and create the req.body object that we will need to access in our routes.

const dbConfig = require("./app/config/db.config.js"); //uses the configuration from db.config.js to create a database connection
var tediousExpress = require('express4-tedious'); //https://www.npmjs.com/package/express4-tedious

const app = express(); //create an Express app

//add body-parser middlewares using app.use() method
app.use(bodyParser.json());  // parse requests of content-type: application/json
app.use(bodyParser.urlencoded({ extended: true })); // parse requests of content-type: application/x-www-form-urlencoded

//initialize middleware in order to add 'sql' method to the request
app.use(function(req, res, next){
  req.sql = tediousExpress({
    server: dbConfig.SERVER,
    authentication: {
        type: 'default',
        options: {
            userName: dbConfig.DB_USER,
            password: dbConfig.DB_PASSWORD
        }
    },
    options: {
        database: dbConfig.DB,
        trustServerCertificate: true,
    }
  });
  next();
});


// simple route
app.get("/rpms/", (req, res) => {
  res.json({ message: "Welcome to the PWADC RPMS Web API." });
});

//Register Routes
require("./app/routes/stores.routes.js")(app); //register the store API routes

var port = process.env.PORT || 3000;

// set port, listen for requests
app.listen(port, () => {
  console.info(`Server is running on port ${port}.`);
});