const { TYPES } = require("tedious");

module.exports = app => {

  app.get('/rpms/stores', function (req, res) {

    req.sql("select * from dbo.stores for json path")
        .into(res);   
  });

  app.get('/rpms/stores/:storeId', function (req, res) {

    var storeId = req.params.storeId;

    req.sql("select top(1) * from dbo.stores where storenumber = @storenumber for json path, without_array_wrapper")
        .param('storenumber', storeId, TYPES.Int)
        .into(res, '{}'); //second parameter is used as default value if nothing is returned from the query
  });
};